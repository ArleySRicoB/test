import { createStore, applyMiddleware, combineReducers, compose } from 'redux';
import thunk from 'redux-thunk';
import usersReducer from './usersReducer';

const mainReducer = combineReducers({ users: usersReducer });

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export default createStore(mainReducer, composeEnhancers(applyMiddleware(thunk)));
