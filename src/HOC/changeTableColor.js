import React, { PureComponent } from 'react';

const changeTableColor = (WrappedComponent) => {
  return class extends PureComponent {
    constructor(props) {
      super(props);
      this.state = {
        color: 'blue',
      };

      this.changeColor = this.changeColor.bind(this);
    }

    changeColor(color) {
      if (color === 'blue') this.setState({ color: 'black' });
      else this.setState({ color: 'blue' });
    }

    render() {
      const { color } = this.state;
      return <WrappedComponent {...this.props} color={color} changeColor={this.changeColor} />;
    }
  };
};

export default changeTableColor;
