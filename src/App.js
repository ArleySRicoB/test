/* eslint-disable import/no-unresolved */
import React from 'react';
import Table from './Containers/Table';
import './styles.scss';

function App() {
  return (
    <div className="App">
      <Table />
    </div>
  );
}

export default App;
