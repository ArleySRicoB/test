import React from 'react';
import PropTypes from 'prop-types';

const Error = ({ error }) => {
  return (
    <div>
      <h2 className="text-center mt-5">{error}</h2>
    </div>
  );
};

Error.propTypes = {
  error: PropTypes.string.isRequired,
};

export default Error;
