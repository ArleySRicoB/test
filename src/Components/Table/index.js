import React from 'react';
import PropTypes from 'prop-types';
import TableList from './TableList';

const TableComponent = ({ users, pending, error, handleClick }) => (
  <div className="container text-center animated fadeIn">
    <h1>ReactJs Developer test</h1>
    <div>
      <button
        className="btn btn-outline-primary col-12 col-md-4 my-4"
        type="button"
        onClick={() => handleClick()}
      >
        Get Users
      </button>
    </div>
    <hr />
    <TableList users={users} pending={pending} error={error} />
  </div>
);

TableComponent.propTypes = {
  handleClick: PropTypes.func.isRequired,
  pending: PropTypes.bool,
  users: PropTypes.oneOfType([PropTypes.array, PropTypes.instanceOf(Object)]).isRequired,
  error: PropTypes.oneOfType([PropTypes.array, PropTypes.instanceOf(Object)]),
};

TableComponent.defaultProps = {
  error: null,
  pending: false,
};

export default TableComponent;
