import React from 'react';
import PropTypes from 'prop-types';
import Loading from '../common/Loading';
import Error from '../common/Error';
import changeTableColor from '../../HOC/changeTableColor';

function Table({ users, pending, error, color, changeColor }) {
  if (pending) return <Loading />;
  if (error) return <Error error="Unable to get users" />;

  return (
    <div className="table-section mt-4 text-left">
      <button className="btn btn-outline-info" type="button" onClick={() => changeColor(color)}>
        Change background color
      </button>
      <table className="table mt-3 text-center">
        <thead style={{ backgroundColor: color }}>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Name</th>
            <th scope="col">Email</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user, index) => (
            <tr key={index}>
              <th>{user.id}</th>
              <th>{user.name}</th>
              <th>{user.email}</th>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

Table.propTypes = {
  pending: PropTypes.bool,
  color: PropTypes.string,
  changeColor: PropTypes.func,
  users: PropTypes.oneOfType([PropTypes.array, PropTypes.instanceOf(Object)]).isRequired,
  error: PropTypes.oneOfType([PropTypes.array, PropTypes.instanceOf(Object)]),
};

Table.defaultProps = {
  error: null,
  pending: false,
  color: 'black',
  changeColor: null,
};

export default changeTableColor(Table);
