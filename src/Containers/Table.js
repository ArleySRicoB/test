import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUsers } from '../Actions/actions';
import TableComponent from '../Components/Table';

class Table extends Component {
  constructor(props) {
    super(props);
    this.handleClick = this.handleClick.bind(this);
  }

  handleClick() {
    const { getUsersAction } = this.props;
    getUsersAction();
  }

  render() {
    const { users, pending, error } = this.props;
    return (
      <TableComponent
        users={users}
        handleClick={this.handleClick}
        pending={pending}
        error={error}
      />
    );
  }
}

const mapStateToProps = ({ users: { users, pending, error } }) => ({ users, pending, error });

const mapDispatchToProps = (dispatch) => ({
  getUsersAction: () => dispatch(getUsers()),
});

Table.propTypes = {
  getUsersAction: PropTypes.func.isRequired,
  pending: PropTypes.bool,
  users: PropTypes.oneOfType([PropTypes.array, PropTypes.instanceOf(Object)]).isRequired,
  error: PropTypes.oneOfType([PropTypes.array, PropTypes.instanceOf(Object)]),
};

Table.defaultProps = {
  error: null,
  pending: false,
};

export default connect(mapStateToProps, mapDispatchToProps)(Table);
