import axios from 'axios';

export const FETCH_USERS_PENDING = 'FETCH_USERS_PENDING';
export const FETCH_USERS_SUCCESS = 'FETCH_USERS_SUCCESS';
export const FETCH_USERS_ERROR = 'FETCH_USERS_ERROR';

function fetchUsersPending() {
  return {
    type: FETCH_USERS_PENDING,
  };
}

function fetchUsersSuccess(users) {
  return {
    type: FETCH_USERS_SUCCESS,
    payload: users,
  };
}

function fetchUsersError(error) {
  return {
    type: FETCH_USERS_ERROR,
    error,
  };
}

export const getUsers = () => {
  const url = process.env.REACT_APP_URL;
  return (dispatch) => {
    dispatch(fetchUsersPending());
    axios
      .get(url)
      .then((response) => {
        if (!response.data || !response.data.data) {
          dispatch(fetchUsersError({ message: 'Data not found' }));
        } else {
          dispatch(fetchUsersSuccess(response.data.data));
        }
      })
      .catch((error) => {
        dispatch(fetchUsersError(error));
      });
  };
};
