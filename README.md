# Get-user-test

Get-user-test is a project to get a list of users from an specified URL and show it in a table, also, you can change the background color of the table titles.

## Starting

### Required technologies

- **Node**

### Install dependencies

In the project root, install the node dependencies with:

```bash
$ npm install
```

### Add environment variables

Duplicate the `.env.sample` file and rename it:

```bash
.env.sample   ->    .env
```

| Name          | Description                |
| ------------- | -------------------------- |
| REACT_APP_URL | URL to get the information |

### Run the application

In the project root, start the server with:

```bash
$ npm run start
```

The application will automatically open in the default browser.
